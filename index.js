const app = require('../util');

const port = 3001;

// Definir o uso das rotas da API de Mock
app.use('/', app.routes.authRoutes);

app.listen(port, () => {
  app.logger.log(`Server listening at http://localhost:${port}`);
});
